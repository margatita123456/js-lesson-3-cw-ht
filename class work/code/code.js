/*Создайте массив размерностью 15 элементов, выведите все элементы в обратном порядке и 
разделите каждый элемент спецсимволом "Облака". 
При загрузке страницы спросите у пользователя индекс и удалите этот элемент из масси*/


/*Создаём массив размерностью на 15 эл-в, используя метод Array*/

const classWork = new Array(15);
classWork [0] = "First";
classWork [1] = "Second";
classWork [2] = "third";
classWork [3] = "fourth";
classWork [4] = "fifth";
classWork [5] = "sixth";
classWork [6] = "seventh";
classWork [7] = "eighth";
classWork [8] = "ninth";
classWork [9] = "tenth";
classWork [10] = "eleventh";
classWork [11] = "twelfth";
classWork [12] = "thirteenth";
classWork [13] = "fourteenth";
classWork [14] = "fifteenth";

/*Выводим элементы нашего массива*/
document.write (classWork);

document.write("<hr/>");

/*Далее нам необходимо создать разделитель, используя спецсимвол "Облако"*/
document.write ("Неободимо вывести значение, которое возвращает метод 'Join':" + "<br/>");
let res = classWork.join("&#9729");  /*Обращаемся к массиву, который вызывается*/ /*При этом массив не изменяется и остаётся полноценным*/
res = classWork.join("<p> &#9729;</p>"); /*элемент, который будет розделять, вместо запятых*/
document.write("<p>" + res);

document.write("<hr/>");

/*Разворачиваем порядок изначального массива*/

classWork.reverse();

document.write(classWork.join(" &#9729;"));





